package ca.abodzay.simplefragment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //adding fragment programmatically
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        val fragment = ExtraFragment()
        fragmentTransaction.add(R.id.main_view, fragment)
        fragmentTransaction.commit()

        setContentView(R.layout.activity_main)
    }

    fun updateFragment1() {
        val fragment = supportFragmentManager.findFragmentById(R.id.example_fragment) as ExampleFragment
        fragment?.setText()
    }
}